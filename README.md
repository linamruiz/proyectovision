<h2> <center> <b> DETECCIÓN DE PÓLIPOS PARA APOYAR EL DIAGNÓSTICO DEL CÁNCER DE COLON </b> </center> </h2>
<h3> <center> <b> Lina Marcela Ruiz García </b></center></h3>

<br>

<h4> <b> INTRODUCCIÓN </b> </h4>

El cáncer de colon es el tercer cáncer más agresivo a nivel mundial, afectando de igual manera a hombres y mujeres. La colonoscopia es el examen estandar para poder realizar su diagnóstico, el cual permite visualizar y remover los principales precursores de la enfermedad denominados pólipos.
<br><br> Los pólipos son masas protuberantes que aparecen en el intestino, dependiendo del estadio en el que se encuentre el paciente este presenta diferente tamaño y forma. Sin embargo presenta una tasa de pérdida del 26% de los pólipos durante una colonoscopia, una cifra bastante alta cuando son los biomarcadores para poder brindar un diagnóstico correcto.


<h4> <b>  OBJETIVO </b> </h4>
Implementar diferentes técnicas de machine learning y deep learning para la detección de pólipos, con el fin de apoyar las tareas de diagnóstico del cáncer de colón.
<br>

<h4> <b> DATASET ASU MAYO CLINIC </b> </h4>  
Disponible en: https://polyp.grand-challenge.org/AsuMayo/<br>

* <b> Train:</b> 20 vídeos (10 con pólipo y 10 sin pólipo) teniendo 18702 frames.
* <b> Test:</b> 18 vídeos (9 con pólipo y 9 sin pólipo) teniendo 17574 frames.


<h4> <b>METODOLOGÍA INICIAL </b> </h4>  
1. <b>ORB_1:</b> Las imágenes se pasan sin ningún filtro. 
<br>
2. <b>CNN:</b> Red convolucional creada a partir del modelo de la VGG16.

<h4> <b> METODOLOGÍA FINAL </b> </h4>  
1. <b>DEEP FEATURES:</b> Extraer de redes convolucionales ya entrenadas como la ResNet50, el maxpooling y usarlo para clasificar las imágenes.
<br>
2.<b>TRANSFER LEARNING:</b> Comparar los resultados con la técnica de transfer learninig y así analizar el mejor resultado.
<br>
3. <b>ORB_2:</b> Clasificar usando keypoints, pero esta vez aprovechando la importancia del color que se tiene en este problema.
<br>

<h4> <b> VÍDEO:</b> </h4> Link: https://drive.google.com/file/d/1CwdWZWAhWxy83FEZ_fB2CnoUaezrmh40/view?usp=sharing
<br>
<h4> <b> PRESENTACIÓN:</b> </h4> Link: https://drive.google.com/file/d/1yw3lrEZ40pOf_k5JOIts6nOr-aOejFyD/view?usp=sharing
<br>
<h4> <b> BANNER:</b> </h4>  El banner se encuentra adjunto con el nombre <b>banner_cancer</b>. 